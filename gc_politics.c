/*
 gc_politics.c - Functions for the Politics section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_social(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Political and Social Issues                                       Page %i of %i\n", PAGES-(PAGES-19), PAGES);
	printf("===============================================================================\n");
        printf("1 PS+++ Legalize drugs! Abolish the government! 'Fuck the draft!'\n");
        printf("2 PS++  I give to liberal causes. I march for gay rights. I'm a card carrying\n");
        printf("        member of the ACLU. Keep abortion safe and legal.\n");
        printf("3 PS+   My whole concept of liberalism is that nobody has the right to tell\n");
        printf("        anybody else what to do, on either side of the political fence. If you\n");
        printf("        don't like it, turn the bloody channel.\n");
        printf("4 PS    I really don't have an opinion; nobody's messing with my freedoms\n");
        printf("        right now.\n");
        printf("5 PS-   Label records! Keep dirty stuff off the TV and the Internet.\n");
        printf("6 PS--  Oppose sex education, abortion rights, gay rights. Rush Limbaugh is\n");
        printf("        my spokesman.\n");
        printf("7 PS--- Repent left-wing sinners and change your wicked evil ways.\n");
        printf("        Buchanan/Robertson in '96.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Political and Social Issues code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_economic(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Politics and Economic Issues                                      Page %i of %i\n", PAGES-(PAGES-20), PAGES);
	printf("===============================================================================\n");
        printf("1 PE+++ Abolish antitrust legislation. Raise taxes on everyone but the rich so\n");
        printf("        that the money can trickle-down to the masses.\n");
        printf("2 PE++  Keep the government off the backs of businesses. Deregulate as much\n");
        printf("        as possible.\n");
        printf("3 PE+   Balance the budget with spending cuts and an amendment.\n");
        printf("4 PE    Distrust both government and business.\n");
        printf("5 PE-   It's ok to increase government spending, so we can help more poor\n");
        printf("        people. Tax the rich! Cut the defense budget!\n");
        printf("6 PE--  Capitalism is evil! Government should provide the services we really\n");
        printf("        need. Nobody should be rich.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Politics and Economic Issues code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 6);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_cypher(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Cypherpunks                                                       Page %i of %i\n", PAGES-(PAGES-21), PAGES);
	printf("===============================================================================\n");
        printf("1 Y+++ I am T.C. May\n");
        printf("2 Y++  I am on the cypherpunks mailing list and active around Usenet. I never\n");
        printf("       an opportunity to talk about the evils of Clipper and ITAR and the NSA.\n");
        printf("       Orwell's 1984 is more than a story, it is a warning to our's and future\n");
        printf("       generations. I'm a member of the EFF.\n");
        printf("3 Y+   I have an interest and concern in privacy issues, but in reality I'm not\n");
        printf("       all that active or vocal.\n");
        printf("4 Y    I'm pretty indifferent on the whole issue.\n");
        printf("5 Y-   It seems to me that all of these concerns are a little extreme. I mean,\n");
        printf("       the government must be able to protect itself from criminals and the\n");
        printf("       populace from indecent speech.\n");
        printf("6 Y--  Get a life. The only people that need this kind of protection are people\n");
        printf("       with something to hide. I think cypherpunks are just a little paranoid.\n");
        printf("7 Y--- I am L. Detweiler\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Cypherpunks code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_pgp(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("PGP                                                               Page %i of %i\n", PAGES-(PAGES-22), PAGES);
	printf("===============================================================================\n");
        printf("1 PGP++++ I am Philip Zimmerman.\n");
        printf("2 PGP+++  I don't send or answer mail that is not encrypted, or at the very\n");
        printf("          least, signed. If you are reading this without decrypting it first,\n");
        printf("          something is wrong. IT DIDN'T COME FROM ME!\n");
        printf("3 PGP++   I have the most recent version and use it regularly.\n");
        printf("4 PGP+    'Finger me for my public key'\n");
        printf("5 PGP     I've used it, but stopped long ago.\n");
        printf("6 PGP-    I don't have anything to hide.\n");
        printf("7 PGP--   I feel that the glory of the Internet is in the anarchic, trusting\n");
        printf("          environment that so nutures the exchange of information. Encryption\n");
        printf("          just bogs that down.\n");
        printf("8 PGP---  If you support encryption on the Internet, you must be a drug dealer\n");
        printf("          or terrorist or something like that.\n");
        printf("9 PGP---- Oh, here is something you can all use that is better\n");
        printf("          (insert Clipper here).\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your PGP code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

