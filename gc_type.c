/*
 gc_type.c - Functions for the Geek Type section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_type(void)
{
   int selection = 99;

   do
     {
	printf("Geek Type                                                          Page %i of %i\n", PAGES-(PAGES-1), PAGES);
	printf("===============================================================================\n");
	printf(" 1 GB  - Geek of Business                15 GL  - Geek of Literature\n");
	printf(" 2 GC  - Geek of Classics                16 GMC - Geek of Mass Communications\n");
	printf(" 3 GCA - Geek of Commercial Arts         17 GM  - Geek of Math\n");
	printf(" 4 GCM - Geek of Computer Management     18 GMD - Geek of Medicine\n");
	printf(" 5 GCS - Geek of Computer Science        19 GMU - Geek of Music\n");
	printf(" 6 GCC - Geek of Communications          20 GPA - Geek of Performing Arts\n");
	printf(" 7 GE  - Geek of Engineering             21 GP  - Geek of Philosophy\n");
	printf(" 8 GED - Geek of Education               22 GS  - Geek of Science\n");
	printf(" 9 GFA - Geek of Fine Arts               23 GSS - Geek of Social Science\n");
	printf("10 GG  - Geek of Government              24 GTW - Geek of Technicial Writing\n");
	printf("11 GH  - Geek of Humanities              25 GO  - Geek of Other\n");
	printf("12 GIT - Geek of Information Technology  26 GU  - Geek of Undecided\n");
	printf("13 GJ  - Geek of Jurisprudence (Law)     27 G!  - Geek of No Qualifications\n");
	printf("14 GLS - Geek of Library Science         28 GAT - Geek of All Trades\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your Geek Type code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 28);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

