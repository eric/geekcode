/*
 geekcode.h - GeekCode Global Header File

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __INCLUDED_GEEKCODE_H__
#define __INCLUDED_GEEKCODE_H__


#define VERSION "1.7.3" /* Added in v1.2 */

#define PAGES 35 /* Page counter: using formula PAGE = PAGES-(PAGES-PAGE)
                    where the second PAGE is the number of the page
                    e.g. PAGE 1 of 35
                    1 = 35 - (35 - 1)
                    in the code:
                    printf("PAGE %i of %i", PAGES-(PAGES-1), PAGES);
                 */

/* Geek type - gc_type.c */
int get_type(void),          gc_type;

/* Appearance section - gc_appearance.c */
int get_dress(void),         gc_dress;
int get_height(void),        gc_height;
int get_weight(void),        gc_weight;
int get_age(void),           gc_age;

/* Computers section - gc_computers.c */
int get_computers(void),     gc_computers;
int get_unix_type(void),     gc_unix_type; /* Added in v1.1 */
int get_unix(int x),         gc_unix;
int get_perl(void),          gc_perl;
int get_linux(void),         gc_linux;
int get_emacs(void),         gc_emacs;
int get_www(void),           gc_www;
int get_usenet(void),        gc_usenet;
int get_oracle(void),        gc_oracle;
int get_kibo(void),          gc_kibo;
int get_windows(void),       gc_windows;
int get_os2(void),           gc_os2;
int get_mac(void),           gc_mac;
int get_vms(void),           gc_vms;

/* Politics section - gc_politics.c */
int get_social(void),        gc_social;
int get_economic(void),      gc_economic;
int get_cypher(void),        gc_cypher;
int get_pgp(void),           gc_pgp;

/* Entertainment section - gc_entertainment.c */
int get_startrek(void),      gc_startrek;
int get_babylon5(void),      gc_babylon5;
int get_xfiles(void),        gc_xfiles;
int get_rp(void),            gc_rp;
int get_television(void),    gc_television;
int get_books(void),         gc_books;
int get_dilbert(void),       gc_dilbert;
int get_doom(void),          gc_doom;
int get_geekcode(void),      gc_geekcode;

/* Lifestyle section - gc_lifestyle.c */
int get_education(void),     gc_education;
int get_housing(void),       gc_housing;
int get_relationships(void), gc_relationships;
int get_sex_type(void),      gc_sex_type; /* Added in v1.2 */
int get_sex(int x),          gc_sex;      /* Modified in v1.2 */
char show_sex_type(int x);                /* Added in v1.2 */

void show_geekcode(int x);                /* Modified in v1.2 */
void write_geekcode(int x);		  /* Added in v1.7 */

void clear_kb(void);                      /* Added in v1.3 */

/* Console I/O - gc_consoleio.c */
void clearscreen();

/* geekcode -> English - gc_translate.c */
void translate(char geekcode[64][16], int j);

#endif /* __INCLUDED_GEEKCODE_H__ */
